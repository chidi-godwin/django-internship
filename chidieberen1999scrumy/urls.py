from django.urls import path, include

from . import views

app_name = "chidieberen1999scrumy"

urlpatterns = [
    path('', views.index, name='index'),
    path('movegoal/<int:goal_id>', views.move_goal, name='movegoal'),
    path('addgoal', views.add_goal, name='addgoal'),
    path('home', views.home, name='home'),
    path('logout', views.logout_view, name='logout'),
    path('accounts/', include('django.contrib.auth.urls')),   
]
