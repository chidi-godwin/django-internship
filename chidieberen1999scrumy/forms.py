from .models import User, ScrumyGoals
from django.forms import ModelForm, Form
from django import forms 

class SignupForm(ModelForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'username', 'password']


class CreateGoalForm(ModelForm):
    class Meta:
        model = ScrumyGoals
        fields = '__all__'


class MoveGoalForm(Form):
    choices = (('Daily Goal', 'Daily Goal'), ('Weekly Goal', 'Weekly Goal'), ('Done Goal', 'Done Goal'), ('Verify Goal', 'Verify Goal'))
    status_name = forms.ChoiceField(choices=choices)
    

